package main

import (
	"context"
	"crypto/tls"
	"github.com/google/uuid"
	"log"
	"net/http"
	"os"
	"slices"
	"strings"
	"time"

	"github.com/tmsmr/xmpp-webhook/parser"
	"mellium.im/sasl"
	"mellium.im/xmpp"
	"mellium.im/xmpp/jid"
	"mellium.im/xmpp/muc"
	"mellium.im/xmpp/mux"
	"mellium.im/xmpp/stanza"
)

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

type MessageBody struct {
	stanza.Message
	Body string `xml:"body"`
}

func initXMPP(address jid.JID, pass string, mucs string) (*xmpp.Session, error) {
	// If XMPP Session cannot be started in 30 seconds, it probably is dead :D
	dialCtx, dialCtxCancel := context.WithTimeout(context.Background(), 30*time.Second)
	xmppSession, err := xmpp.DialClientSession(dialCtx, address,
		xmpp.BindResource(), // Generates random resourcepart (ie. the /client in a@b.com/client)
		xmpp.StartTLS(&tls.Config{
			ServerName: address.Domain().String(), // extract domain from JID
			MinVersion: tls.VersionTLS12,          // don't connect if server doesn't have TLS1.2 or higher
		}),
		// sasl.* are algorithms to allow negotiation with for login.
		xmpp.SASL("", pass, sasl.ScramSha1Plus, sasl.ScramSha1, sasl.Plain),
	)
	if err != nil {
		log.Fatalf("Error loging in: %v\n", err)
		os.Exit(1)
	}
	// If xmppSession finishes before 30 seconds, it will proceed to this statement which will cancel the 30 second timer.
	dialCtxCancel()

	mucClient := &muc.Client{}
	mux := mux.New("jabber:client", muc.HandleClient(mucClient))

	go func() {
		err := xmppSession.Serve(mux)
		if err != nil {
			log.Fatalf("error handling xmppSession responses: %v", err)
		}
	}()

	for _, r := range strings.Split(mucs, ",") {
		parsedToAddr, err := jid.Parse(r)
		panicOnErr(err)
		roomJID, _ := parsedToAddr.WithResource(strings.Split(address.String(), "@")[0])
		opts := []muc.Option{muc.MaxBytes(0)}
		_, err = mucClient.Join(context.TODO(), roomJID, xmppSession, opts...)
		if err != nil {
			log.Fatalf("error joining MUC %s: %v", err)
		}
	}
	return xmppSession, nil
}

func closeXMPP(session *xmpp.Session) {
	_ = session.Close()
	_ = session.Conn().Close()
}

func main() {
	// get xmpp credentials, message recipients
	xi := os.Getenv("XMPP_ID")
	xp := os.Getenv("XMPP_PASS")
	xr := os.Getenv("XMPP_RECIPIENTS")
	xm := os.Getenv("XMPP_MUC_RECIPIENTS")

	// get listen address
	listenAddress := os.Getenv("XMPP_WEBHOOK_LISTEN_ADDRESS")
	if len(listenAddress) == 0 {
		listenAddress = ":4321"
	}

	// check if xmpp credentials and recipient list are supplied
	if xi == "" || xp == "" || xr == "" {
		log.Fatal("XMPP_ID, XMPP_PASS or XMPP_RECIPIENTS not set")
	}

	myjid, err := jid.Parse(xi)
	panicOnErr(err)

	// connect to xmpp server
	xmppSession, err := initXMPP(myjid, xp, xm)
	panicOnErr(err)
	defer closeXMPP(xmppSession)

	// send initial presence
	panicOnErr(xmppSession.Send(context.TODO(), stanza.Presence{Type: stanza.AvailablePresence}.Wrap(nil)))

	log.Println("XMPP Booted up")

	// create chan for messages (webhooks -> xmpp)
	messages := make(chan string)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// wait for messages from the webhooks and send them to all recipients
	go func() {
		for m := range messages {
			for _, r := range strings.Split(xr, ",") {
				recipient, err := jid.Parse(r)
				panicOnErr(err)
				// try to send message, ignore errors
				mucs := strings.Split(xm, ",")
				var msgType stanza.MessageType
				if slices.Contains(mucs, r) {
					msgType = stanza.GroupChatMessage
				} else {
					msgType = stanza.ChatMessage
				}
				_ = xmppSession.Encode(ctx, MessageBody{
					Message: stanza.Message{
						ID:   uuid.New().String(),
						To:   recipient,
						From: myjid,
						Type: msgType,
					},
					Body: m,
				})
			}
		}
	}()
	// initialize handlers with associated parser functions
	http.Handle("/grafana", newMessageHandler(messages, parser.GrafanaParserFunc))
	http.Handle("/shoutrrr", newMessageHandler(messages, parser.ShoutrrrParserFunc))
	http.Handle("/slack", newMessageHandler(messages, parser.SlackParserFunc))
	http.Handle("/alertmanager", newMessageHandler(messages, parser.AlertmanagerParserFunc))

	// listen for requests
	_ = http.ListenAndServe(listenAddress, nil)
}
