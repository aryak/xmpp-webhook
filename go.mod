module github.com/tmsmr/xmpp-webhook

require (
	github.com/google/uuid v1.6.0
	mellium.im/sasl v0.3.1
	mellium.im/xmlstream v0.15.4
	mellium.im/xmpp v0.21.4
)

require (
	golang.org/x/crypto v0.18.0 // indirect
	golang.org/x/mod v0.14.0 // indirect
	golang.org/x/net v0.20.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/tools v0.17.0 // indirect
	mellium.im/reader v0.1.0 // indirect
)

go 1.18
