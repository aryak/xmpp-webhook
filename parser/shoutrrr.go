package parser

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

func ShoutrrrParserFunc(r *http.Request) (string, error) {
	// get alert data from request
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return "", errors.New(readErr)
	}

	alert := &struct {
		Title   string `json:"title"`
		Message string `json:"message"`
	}{}

	// parse body into the alert struct
	err = json.Unmarshal(body, &alert)
	if err != nil {
		return "", errors.New(parseErr)
	}

	// construct alert message
	var message string
	if alert.Title != "" {
		message = "Title: " + alert.Title + "\n"
	}
	message += alert.Message
	return message, nil
}
