# xmpp-webhook
- Multipurpose XMPP-Webhook (Built for DevOps Alerts)
- Based on https://github.com/mellium/xmpp

## Status
`xmpp-webhook` currently support:

- Grafana Webhook alerts
- Alertmanager Webhooks
- Slack Incoming Webhooks (Feedback appreciated)
- Shoutrrr Alerts

Check https://codeberg.org/aryak/xmpp-webhook/blob/master/parser/ to learn how to support more source services.

## Usage
- `xmpp-webhook` is configured via environment variables:
    - `XMPP_ID` - The JID we want to use
    - `XMPP_PASS` - The password
    - `XMPP_RECIPIENTS` - Comma-separated list of JID's
    - `XMPP_MUC_RECIPIENTS` - Comma-separated list of MUCs to send messages to (these MUCs also need to be in `XMPP_RECIPIENTS`)
    - `XMPP_WEBHOOK_LISTEN_ADDRESS` - Bind address (Optional)
- After startup, `xmpp-webhook` tries to connect to the XMPP server and provides the implemented HTTP enpoints. e.g.:

```
curl -X POST -d @dev/grafana-webhook-alert-example.json localhost:4321/grafana
curl -X POST -d @dev/alertmanager-example.json localhost:4321/alertmanager
curl -X POST -d @dev/slack-compatible-notification-example.json localhost:4321/slack
curl -X POST -d @dev/shoutrrr-example.json localhost:4321/shoutrrr
```
- After parsing the body in the appropriate `parserFunc`, the notification is then distributed to the configured recipients.

## Run with Docker
### Build it
- Build image: `docker build -t xmpp-webhook .`
- Run: `docker run -e "XMPP_ID=alerts@example.org" -e "XMPP_PASS=xxx" -e "XMPP_RECIPIENTS=a@example.org,b@example.org,room@muc.example.com" -e "XMPP_MUC_RECIPIENTS=room@muc.example.com" -p 4321:4321 -d --name xmpp-webhook xmpp-webhook`
### Use prebuilt image from Docker Hub
- Run: `docker run -e "XMPP_ID=alerts@example.org" -e "XMPP_PASS=xxx" -e "XMPP_RECIPIENTS=a@example.org,b@example.org,room@muc.example.com" -e "XMPP_MUC_RECIPIENTS=room@muc.example.com" -p 4321:4321 -d --name xmpp-webhook codeberg.org/aryak/xmpp-webhook:latest`
