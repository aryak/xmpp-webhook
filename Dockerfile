FROM --platform=$BUILDPLATFORM golang:alpine AS build
MAINTAINER Arya Kiran <me@aryak.me>

ARG TARGETARCH

WORKDIR /src
RUN apk --no-cache add git
COPY . .

RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux GOARCH=$TARGETARCH go build -o /src/xmpp-webhook

FROM alpine:3.16 as bin

RUN apk add --no-cache ca-certificates

WORKDIR /app
COPY --from=build /src/xmpp-webhook .

ENV XMPP_ID="" \
	XMPP_PASS="" \
	XMPP_RECIPIENTS="" \
	XMPP_MUC_RECIPIENTS=""

EXPOSE 4321

CMD ["/app/xmpp-webhook"]
